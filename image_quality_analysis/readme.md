# Quantitative and Qualitative Metrics

* This repository contains notebooks responsible for calculating the quantitative metrics of the generated images and also for selecting the best images (according to the metrics) to be used in the qualitative analysis web application.

* You need to perform the quantitative analysis experiments to get the qualitative analysis data.

* Before executing the synthetic image quality analysis codes, it is necessary to generate new images with StyleGAN2ADA (see [stylegan2ada.ipynb](../stylegan/stylegan2ada.ipynb)).

## Directory Organization

|               Filename                |                                   Description                                    |
|:-------------------------------------:|:--------------------------------------------------------------------------------:|
| [generated_images/](generated_images) | Directory that contains generated images with different p-values for each class  |
|     [quantitative/](quantitative)     |             Directory that contains the quantitative analysis code.              |
|      [qualitative/](qualitative)      |              Directory that contains the qualitative analysis code.              |
