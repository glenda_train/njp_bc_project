# Web Application of Qualitative Analysis

* This repository contains JavaScript codes to build an application to perform the qualitative analysis of the generated images.

* The focus of the research was not on building a robust web application, because it was defined that pathologists would use the application in offline mode.

* In this application, two qualitative protocols are considered:

    * **Rapid Scene Categorization:**
        * Where the participant has 3 seconds to say whether an image is real or generated.
        * 96 images were considered.
        * 12 generated images of each class.
        * 12 real images of each class.

        <br />

    * **Judgement By Preference:**
        * Where the participant observes each generated image, with no time limit, and reports the quality of the image (bad | medium | good).
        * 100 generated images were considered.

## [ViteJS](https://vitejs.dev/)
* To access the application through the browser, you must have **Node JS** installed.
* After installing Node JS, you can run the commands below in the terminal (open in the path of this folder) and access the address informed in your browser:
```
npm install
npm run dev
```
* Example: http://localhost:5173/

## How to Use

* After opening the informed address, the application will display an initial page, asking the participant to indicate its name.

![initial page](readme_images/initial_page.png)

* Then, the next step is the selection of the protocol that the participant wants to contribute.
* There are two options, which are shown in the image below.

![initial page](readme_images/protocol_choices_page.png)

* If you choose the **Rapid Scene Categorization (CRC)** protocol, then you will see this screen with the protocol information:

![initial page](readme_images/crc_protocol.png)

* In this protocol, the image will be shown for 3 seconds and, after that time, you will have to press one of the two buttons indicating if the image is real ou generated.

![initial page](readme_images/crc_example_1.png)
![initial page](readme_images/crc_example_2.png)

* If you choose the **Judgement by Preference (JPP)** protocol, then you will see this screen with the protocol information: 

![initial page](readme_images/jpp_protocol.png)

* In this protocol, the image will be shown on the screen (with no time limit) and you will have to press one of the three buttons indicating the quality of the generated image (bad | medium | good).

![initial page](readme_images/jpp_example.png)

* There are 100 imagens in each protocol. When you finished seeing the images, the application will show the end page below and save the answers inside a JSON.

![initial page](readme_images/end_page.png)







