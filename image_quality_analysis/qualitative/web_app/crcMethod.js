// JS responsible for dealing with the flow of the Fast Scene Categorization protocol (CRC)

import { crcImagesData as imagesData } from "./utilities";
import { saveInLocalStorage, renderEndPage, readFromLocalStorage, saveDataJSON } from "./utilities";

let currentImageId = 1;
let timeout = 3000;

// Function that shows the chosen protocol on the screen (CRC in this case)
function showMethodProtocol() {

    // Build the page (method protocol)
    const form = document.getElementsByTagName("main")[0];
    form.innerHTML = 
    `
    <section id="crc-protocol" class="flex flex-col justify-center items-center h-[calc(100vh-88px)] ml-8 mr-8">
      <div class="flex flex-col gap-3 bg-white rounded-xl p-10">
        <p class="font-bold text-center text-3xl pb-6">Protocolo de Categorização Rápida de Cenas</p>
        <p><strong>1)</strong> Foram escolhidas <strong>96 imagens</strong> de Imunohistoquímica de pacientes avaliados para Câncer de Mama.</p>
        <p><strong>2)</strong> Essas imagens são referentes ao teste para os Receptores de Estrogênio (RE).</p>
        <p><strong>3)</strong> Cada uma dessas imagens pode ser <strong>Real</strong> ou <strong>Gerada</strong>.</p>
        <p><strong>4)</strong> As <strong>Imagens Reais</strong> são imagens extraídas das lâminas dos pacientes, advindas do conjunto de dados.</p>
        <p><strong>5)</strong> As <strong>Imagens Geradas</strong> são imagens sintéticas obtidas com um modelo generativo.</p>
        <p><strong>6)</strong> Cada imagem será apresentada na tela por <strong>3 segundos</strong>.</p>
        <p><strong>7)</strong> O avaliador deverá <strong>indicar</strong> se a imagem apresentada é uma imagem Real ou Gerada.</p>
        <p><strong>8)</strong> A próxima imagem só será apresentada depois da resposta ser indicada.</p>
        <p><strong>9)</strong> A avaliação termina quando todas as 96 imagens forem avaliadas.</p>
        <div class="flex flex-col justify-center items-center">
          <button id="start-crc-button" class="text-lg bg-slate-950 text-slate-200 font-bold rounded-xl h-12 w-48 mt-5 hover:bg-slate-600 hover:text-slate-200">Começar</button>
        </div>
      </div>
    </section>
    `
    document.getElementById("start-crc-button").addEventListener("click", resetTimeout);
    document.getElementById("start-crc-button").addEventListener("click", initialRender);
}    
    
// Function that controls all the flow to present the images and the buttons and deal with timeout (3 secs)
function initialRender() {

    // Shows the end page when all images are annotated and saves the answers inside a JSON
    if(currentImageId === imagesData.length + 1) {
        console.log("debug: cleaned last timeout!");
        clearTimeout(timerId);
        renderEndPage();

        const userName = readFromLocalStorage("userName");
        const method = readFromLocalStorage("method");
        const date = new Date().toLocaleDateString("pt-BR", {
            hour: "2-digit",
            minute: "2-digit"
        });

        saveDataJSON(`avaliacao_qualitativa_${userName}_${method}_${date}.json`, imagesData);
        return;
    }

    renderImage(currentImageId);
    document.getElementById("button-generated-crc").addEventListener("click", resetTimeout);
    document.getElementById("button-real-crc").addEventListener("click", resetTimeout);
    document.getElementById("button-generated-crc").addEventListener("click", () => changeImage("Gerada"));
    document.getElementById("button-real-crc").addEventListener("click", () => changeImage("Real"));
}

// Function that controls the image switch after a button is pressed
function changeImage(imageType) {
    const imageData = imagesData.find(d => d.id === currentImageId);;
    imageData.type = imageType;

    saveInLocalStorage("response", imagesData);

    currentImageId++;
    initialRender();
}

// Function that render the new image and its buttons
function renderImage(idImage) {
    
    // Find the image by id
    const imageData = imagesData.find(d => d.id === idImage);
    console.log(imageData.filename);

    // Find the section to update
    const methodChoices = document.getElementById("crc-protocol");
    
    // Update it
    methodChoices.innerHTML = 
    `
    <section id="crc-avaliation" class="flex flex-col items-center justify-center h-[calc(100vh-92px)] w-3/12">
        <p class="text-lg pb-2">${imageData.id}/${imagesData.length}</p>
        <div id="bg-image" class="h-[256px] w-[256px]">
            <img id="crc-image-${imageData.id}" src="./assets/crc_images/${imageData.filename}" alt="Imagem a ser avaliada." class="h-full w-full border-2 border-slate-400">
        </div>
        <div id="crc-type-buttons" class="flex justify-center gap-3 text-xl mt-6">
            <button id="button-generated-crc" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-52 hover:bg-slate-600 hover:text-slate-200">Gerada</button>
            <button id="button-real-crc" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-52 hover:bg-slate-600 hover:text-slate-200">Real</button>
        </div>
    </section>
    `;
}

// Function that replaces the image by a white screen when the timeout is reached
function timeoutReached()
{
    console.log("debug: white screen loaded!");
    const methodChoices = document.getElementById("bg-image");
    methodChoices.innerHTML = `
      <p class="flex h-full w-full text-center justify-center items-center bg-white p-10 text-xl font-bold">Informe sua Resposta para Prosseguir!</p>
    `;  
}

// Function that reset the timeout when a button is pressed
let timerId = null;
function resetTimeout() {
    if (timerId != null) {
        clearTimeout(timerId);
    }   
    console.log("debug: resetting timeout!");
    timerId = setTimeout(timeoutReached, timeout);
}

showMethodProtocol();