// JS responsible for dealing with the flow of the Judgement by Preference protocol (JPP)

import { jppImagesData as imagesData } from "./utilities";
import { readFromLocalStorage, saveInLocalStorage, saveDataJSON, renderEndPage } from "./utilities";

let currentImageId = 1;

// Function that shows the chosen protocol on the screen (JPP in this case)
function showMethodProtocol() {

    // Build the page (method protocol)
    const form = document.getElementsByTagName("main")[0];
    form.innerHTML = 
    `
    <section id="crc-protocol" class="flex flex-col justify-center items-center h-[calc(100vh-88px)] ml-8 mr-8">
      <div class="flex flex-col gap-3 bg-white rounded-xl p-10">
        <p class="font-bold text-center text-3xl pb-6">Protocolo de Julgamento por Preferência</p>
        <p><strong>1)</strong> Foram escolhidas <strong>100 imagens geradas</strong> de Imunohistoquímica referentes aos biomarcadores de Receptores de Estrogênio (RE).</p>
        <p><strong>2)</strong> Cada uma dessas imagens pode ser classificada <strong>Boa</strong>, <strong>Média</strong> ou <strong>Ruim</strong> em relação à qualidade da imagem gerada.</p>
        <p><strong>3)</strong> Cada imagem será apresentada na tela até uma dessas classes ser associada à imagem.</p>
        <p><strong>4)</strong> O avaliador deverá <strong>indicar</strong> se a imagem apresentada tem uma qualidade boa, média ou ruim em relação às características de imagens reais.</p>
        <p><strong>5)</strong> Após a indicação, a imagem seguinte será apresentada.</p>
        <p><strong>6)</strong> A avaliação termina quando todas as 100 imagens forem avaliadas.</p>
        <div class="flex flex-col justify-center items-center">
          <button id="start-crc-button" class="text-lg bg-slate-950 text-slate-200 font-bold rounded-xl h-12 w-48 mt-5 hover:bg-slate-600 hover:text-slate-200">Começar</button>
        </div>
      </div>
    </section>
    `

    document.getElementById("start-crc-button").addEventListener("click", initialRender);
}

// Function that controls all the flow to present the images and the buttons (Good, Medium and Bad)
function initialRender() {
    
    // Shows the end page when all images are annotated and saves the answers inside a JSON
    if(currentImageId === imagesData.length + 1) {
        renderEndPage();

        const userName = readFromLocalStorage("userName");
        const method = readFromLocalStorage("method");
        const date = new Date().toLocaleDateString("pt-BR", {
            hour: "2-digit",
            minute: "2-digit"
        });

        saveDataJSON(`avaliacao_qualitativa_${userName}_${method}_${date}.json`, imagesData);
        return;
    }

    renderImage(currentImageId);
    document.getElementById("button-bad").addEventListener("click", () => changeImage("Ruim"));
    document.getElementById("button-medium").addEventListener("click", () => changeImage("Média"));
    document.getElementById("button-good").addEventListener("click", () => changeImage("Boa"));
}

// Function that controls the image switch after a button is pressed
function changeImage(imageType) {
    const imageData = imagesData.find(d => d.id === currentImageId);;
    imageData.type = imageType;

    saveInLocalStorage("response", imagesData);

    currentImageId++;
    initialRender();
}

// Function that render the new image and its buttons
function renderImage(idImage) {
    
    // Find the image by id
    const imageData = imagesData.find(d => d.id === idImage);
    console.log(imageData.filename);

    // Find the section to update
    const methodChoices = document.getElementById("crc-protocol");
    
    // Update it
    methodChoices.innerHTML = 
    `
    <section id="crc-avaliation" class="flex flex-col items-center justify-center h-[calc(100vh-88px)]">
      <p class="text-lg pb-2">${imageData.id}/${imagesData.length}</p>
      <div id="bg-image" class="h-[256px] w-[256px]">
          <img id="crc-image-${imageData.id}" src="./assets/jpp_images/${imageData.filename}" alt="Imagem a ser avaliada." class=" h-full border-2 border-slate-400 top-0">
      </div>
      <div id="crc-type-buttons" class="flex justify-center gap-5 text-xl mt-6">
          <button id="button-bad" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-32 hover:bg-slate-600 hover:text-slate-200">Ruim</button>
          <button id="button-medium" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-32 hover:bg-slate-600 hover:text-slate-200">Média</button>
          <button id="button-good" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-32 hover:bg-slate-600 hover:text-slate-200">Boa</button>
      </div>
    </section>
    `;
}

showMethodProtocol();