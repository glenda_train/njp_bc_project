# Classification of Estrogen Receptors (ER) In Relation to IS Score

* This repository contains codes to classify Estrogen Receptor biomarker patches in relation to the **IS score classes**, which are:

    * **0**  &ensp; | &ensp;Negative
    * **1+** &nbsp;| &ensp;Weakly Positive
    * **2+** &nbsp;| &ensp;Moderately Positive
    * **3+** &nbsp;| &ensp;Strongly Positive
   
    <br />

* To carry out the classification, 4 different models were used, called Rogalsky Methodology, Proposed CNN, DenseNet and ViT.

## Rogalsky Methodology

* This method relied on image processing, feature extraction and machine learning techniques to perform the classification.

* The method is presented in the figure below and more information can be obtained in this [notebook](svm.ipynb).

* The code was inspired by [Rogalsky](https://acervodigital.ufpr.br/handle/1884/73470).

![SVM flow](../readme_images/svm_flow.png "SVM Flow")

## Proposed CNN

* To automate the feature extraction step, we propose a CNN based on the architecture of [Tang et. al](https://doi.org/10.1038/s41467-019-10212-1).

* The method is presented in the figure below and more information can be obtained in this [notebook](cnn.ipynb).

## DenseNet

* To explore recent CNN architectures, we implemented a DenseNet121.

* The method is presented in the figure below and more information can be obtained in this [notebook](densenet.ipynb).

## ViT

* To compare the result of a transformer-based model with CNNs, we implemented a Vision Transformer (ViT).

* The method is presented in the figure below and more information can be obtained in this [notebook](vit.ipynb).

![All Nets Flow](../readme_images/all_nets_flow.png "Flow of Models Based on Neural Networks")


## File Organization

|      Filename          |                                   Description                                    |
|:----------------------:|:--------------------------------------------------------------------------------:|
| [svm.ipynb](svm.ipynb) |  Notebook that contains all the steps of the Rogalsky Methodology. |
| [cnn.ipynb](cnn.ipynb) |  Notebook that contains all the steps of the Proposed CNN. |
| [densenet.ipynb](densenet.ipynb) |  Notebook that contains all the steps of the DensetNet121. |
| [vit.ipynb](vit.ipynb) |  Notebook that contains all the steps of the ViT. |
| [metric_utils.py](metric_utils.py) | Script that contains the functions for calculation classification metrics. |
| [log_utils.py](log_utils.py) | Script that contains the log funtions. |


## References

*Rogalsky, J. E. (2021). Semi-automatic ER and PR scoring in immunohistochemistry H-DAB breast cancer images. https://acervodigital.ufpr.br/handle/1884/73470.

*Tang, Z., Chuang, K.V., DeCarli, C. et al. Interpretable classification of Alzheimer’s disease pathologies with a convolutional neural network pipeline. Nat Commun 10, 2173 (2019). https://doi.org/10.1038/s41467-019-10212-1

