# Qualitative Analysis

* To increase the robustness of our research, we implemented a qualitative analysis that takes into account 2 protocols to select the images: **Rapid Scene Categorization (CRC)** and **Judgement By Preference (JPP)**.

* In CRC, the partipant observes an image for 3 seconds and must inform whether the image is real or generated by an AI.

* In JPP, the participant will evaluate the quality of the images, saying whether a generated image is bad, medium or good.

* For CRC, the default is the selection of 96 images, with 12 generated images from each class (48 generated images in total) and 12 real images of each class (48 real images in total).

* For JPP, the default is the selection of 100 images, with 25 generated images from each class. 

* So, the [select_best_images.ipynb](select_best_images.ipynb) notebook is responsible for separating these images, which will be delivered to a web application later.

* To learn more about the web interface, click [here](web_app/readme.md).

## File Organization

|                          Filename                           |                                                           Description                                                           |
|:-----------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------:|
|    [select_best_images.ipynb](select_best_images.ipynb)     |               File that selects the best generated images according to the result of quantitative analysis code.                |
| [create_list_of_images.ipynb](create_list_of_images.ipynb)  | File that generates a list of dictionaries with the ids and names of the selected images to be used within the web application. |
| [result_analysis.ipynb](result_analysis.ipynb)  | File with analysis of the results of experts' responses. |



## Directory Organization
 
|                  Filename                  |                                      Description                                       |
|:------------------------------------------:|:--------------------------------------------------------------------------------------:|
|             [web_app](web_app)             |    Directory that contains the web application to obtain the qualitative analysis.     |
| [specialists_answers](specialists_answers) | Directory that contains the answers from the 3 specialists, obtained from the web app. |
