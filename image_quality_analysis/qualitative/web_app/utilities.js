// JS common for all codes

// Funtion that saves an information inside the browser local storage
export function saveInLocalStorage(key, info) {
    localStorage.setItem(key, JSON.stringify(info));
}

// Function that load an information inside the browser local storage
export function readFromLocalStorage(key) {
    return(JSON.parse(localStorage.getItem(key)));
}

// Function that remove an information from the local storage
export function removeFromLocalStorage(key) {
    localStorage.removeItem(key);
}

// Function that saves the data inside a JSON
export function saveDataJSON(filename, imagesData) {

    const dataToJSON = [ {"userName": readFromLocalStorage("userName")}, 
                   {"method": readFromLocalStorage("method")}, 
                   imagesData ];

    var element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(JSON.stringify(dataToJSON)));
    element.setAttribute("download", filename);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

// Function to render the end page
export function renderEndPage() {
    const endPage = document.getElementsByTagName("main")[0];
    endPage.innerHTML = 
    `
    <section id="crc-protocol" class="flex flex-col justify-center items-center h-[calc(100vh-88px)] ml-8 mr-8">
      <div class="flex flex-col gap-3 bg-white rounded-xl p-10">
        <p class="text-center text-3xl pb-6">Suas respostas foram salvas!</p> 
        <p class="font-bold text-center text-3xl pb-6">Obrigada!</p> 
      </div>
    </section>`;
}

// CRC data
export var crcImagesData = [
    { id: 1, filename: "2_seed0400.png", type: "", }, 
    { id: 2, filename: "0_seed0802.png", type: "", }, 
    { id: 3, filename: "3_seed0642.png", type: "", }, 
    // { id: 4, filename: "020RE_s7c0x130615-1600y117590-1200m626.png", type: "", }, Replaced due to data privacy
    // { id: 5, filename: "457RE_s1c0x111491-1600y72842-1200m2667.png", type: "", }, Replaced due to data privacy
    // { id: 6, filename: "182RE_s0c0x90338-1600y61044-1200m7188.png", type: "", }, Replaced due to data privacy
    // { id: 7, filename: "448RE_s1c0x116493-1600y49287-1200m1005.png", type: "", }, Replaced due to data privacy
    { id: 8, filename: "3_seed0718.png", type: "", }, 
    { id: 9, filename: "2_seed0184.png", type: "", }, 
    // { id: 10, filename: "520RE_s2c0x150678-1600y28975-1200m1418.png", type: "", }, Replaced due to data privacy
    { id: 11, filename: "1_seed0713.png", type: "", }, 
    { id: 12, filename: "3_seed0889.png", type: "", }, 
    { id: 13, filename: "0_seed0889.png", type: "", }, 
    // { id: 14, filename: "020RE_s7c0x132039-1600y117590-1200m627.png", type: "", }, Replaced due to data privacy
    { id: 15, filename: "0_seed0469.png", type: "", }, 
    // { id: 16, filename: "864RE_s1c0x77679-1600y72874-1200m1876.png", type: "", }, Replaced due to data privacy
    // { id: 17, filename: "251RE_s0c0x123426-1600y76077-1200m1898.png", type: "", }, Replaced due to data privacy
    { id: 18, filename: "2_seed0908.png", type: "", }, 
    // { id: 19, filename: "520RE_s2c0x116409-1600y15008-1200m511.png", type: "", }, Replaced due to data privacy
    { id: 20, filename: "1_seed0908.png", type: "", }, 
    // { id: 21, filename: "051RE_s1c0x225757-1600y127531-1200m5632.png", type: "", }, Replaced due to data privacy
    // { id: 22, filename: "182RE_s0c0x88921-1600y66410-1200m7826.png", type: "", }, Replaced due to data privacy
    // { id: 23, filename: "182RE_s0c0x90337-1600y59972-1200m7061.png", type: "", }, Replaced due to data privacy
    { id: 24, filename: "1_seed0856.png", type: "", }, 
    { id: 25, filename: "3_seed0071.png", type: "", }, 
    { id: 26, filename: "0_seed0400.png", type: "", }, 
    { id: 27, filename: "0_seed0713.png", type: "", }, 
    // { id: 28, filename: "451RE_s1c0x177697-1600y101824-1200m13340.png", type: "", }, Replaced due to data privacy
    // { id: 29, filename: "022RE_s5c0x90714-1600y165032-1200m587.png", type: "", }, Replaced due to data privacy
    { id: 30, filename: "2_seed0713.png", type: "", }, 
    { id: 31, filename: "0_seed0642.png", type: "", }, 
    { id: 32, filename: "1_seed0184.png", type: "", }, 
    { id: 33, filename: "2_seed0718.png", type: "", }, 
    // { id: 34, filename: "480RE_s2c0x86520-1600y65602-1200m906.png", type: "", }, Replaced due to data privacy
    // { id: 35, filename: "534RE_s0c0x135409-1600y98584-1200m3579.png", type: "", }, Replaced due to data privacy
    // { id: 36, filename: "182RE_s0c0x74618-1600y29950-1200m3410.png", type: "", }, Replaced due to data privacy
    { id: 37, filename: "3_seed0184.png", type: "", }, 
    // { id: 38, filename: "662RE_s1c0x136550-1600y10623-1200m345.png", type: "", }, Replaced due to data privacy
    { id: 39, filename: "2_seed0802.png", type: "", }, 
    { id: 40, filename: "2_seed0856.png", type: "", }, 
    // { id: 41, filename: "400RE_s0c0x230939-1600y26765-1200m2025.png", type: "", }, Replaced due to data privacy
    // { id: 42, filename: "366RE_s0c0x153930-1600y77182-1200m3345.png", type: "", }, Replaced due to data privacy
    // { id: 43, filename: "020RE_s7c0x127763-1600y109014-1200m166.png", type: "", }, Replaced due to data privacy
    { id: 44, filename: "1_seed0469.png", type: "", }, 
    { id: 45, filename: "3_seed0908.png", type: "", }, 
    // { id: 46, filename: "470RE_s0c0x159653-1600y12842-1200m117.png", type: "", }, Replaced due to data privacy
    { id: 47, filename: "1_seed0071.png", type: "", }, 
    // { id: 48, filename: "534RE_s0c0x131110-1600y96426-1200m3486.png", type: "", }, Replaced due to data privacy
    { id: 49, filename: "0_seed0856.png", type: "", }, 
    // { id: 50, filename: "470RE_s0c0x156805-1600y11773-1200m102.png", type: "", }, Replaced due to data privacy
    // { id: 51, filename: "058RE_s2c0x152874-1600y66454-1200m2995.png", type: "", }, Replaced due to data privacy
    { id: 52, filename: "1_seed0889.png", type: "", }, 
    // { id: 53, filename: "182RE_s0c0x94621-1600y65335-1200m7703.png", type: "", }, Replaced due to data privacy
    // { id: 54, filename: "699RE_s1c0x118086-1600y56776-1200m2394.png", type: "", }, Replaced due to data privacy
    { id: 55, filename: "2_seed0616.png", type: "", }, 
    // { id: 56, filename: "480RE_s2c0x86518-1600y66670-1200m953.png", type: "", }, Replaced due to data privacy
    { id: 57, filename: "0_seed0071.png", type: "", }, 
    // { id: 58, filename: "182RE_s0c0x78915-1600y43908-1200m5138.png", type: "", }, Replaced due to data privacy
    // { id: 59, filename: "380RE_s1c0x158246-1600y29969-1200m1599.png", type: "", }, Replaced due to data privacy
    // { id: 60, filename: "363RE_s1c0x165450-1600y5338-1200m47.png", type: "", }, Replaced due to data privacy
    { id: 61, filename: "3_seed0713.png", type: "", }, 
    { id: 62, filename: "0_seed0184.png", type: "", }, 
    // { id: 63, filename: "693RE_s4c0x99216-1600y125153-1200m292.png", type: "", }, Replaced due to data privacy
    // { id: 64, filename: "020RE_s7c0x139184-1600y119752-1200m794.png", type: "", }, Replaced due to data privacy
    // { id: 65, filename: "331RE_s3c0x117861-1600y53488-1200m4336.png", type: "", }, Replaced due to data privacy
    // { id: 66, filename: "520RE_s2c0x152104-1600y24698-1200m1166.png", type: "", }, Replaced due to data privacy
    // { id: 67, filename: "864RE_s1c0x100496-1600y51426-1200m1283.png", type: "", }, Replaced due to data privacy
    { id: 68, filename: "1_seed0616.png", type: "", }, 
    { id: 69, filename: "3_seed0802.png", type: "", }, 
    { id: 70, filename: "2_seed0642.png", type: "", }, 
    { id: 71, filename: "2_seed0071.png", type: "", }, 
    { id: 72, filename: "1_seed0802.png", type: "", }, 
    { id: 73, filename: "1_seed0400.png", type: "", }, 
    // { id: 74, filename: "182RE_s0c0x77489-1600y42838-1200m5005.png", type: "", }, Replaced due to data privacy
    // { id: 75, filename: "460RE_s0c0x119834-1600y59999-1200m2608.png", type: "", }, Replaced due to data privacy
    // { id: 76, filename: "073RE_s1c0x136326-1600y98604-1200m3681.png", type: "", }, Replaced due to data privacy
    { id: 77, filename: "0_seed0908.png", type: "", }, 
    // { id: 78, filename: "020RE_s7c0x130615-1600y116516-1200m552.png", type: "", }, Replaced due to data privacy
    { id: 79, filename: "2_seed0469.png", type: "", }, 
    // { id: 80, filename: "470RE_s0c0x158232-1600y8560-1200m65.png", type: "", }, Replaced due to data privacy
    // { id: 81, filename: "051RE_s1c0x228620-1600y128599-1200m5681.png", type: "", }, Replaced due to data privacy
    { id: 82, filename: "3_seed0616.png", type: "", }, 
    { id: 83, filename: "1_seed0642.png", type: "", }, 
    // { id: 84, filename: "101RE_s0c0x70639-1600y67506-1200m4184.png", type: "", }, Replaced due to data privacy
    { id: 85, filename: "1_seed0718.png", type: "", }, 
    // { id: 86, filename: "699RE_s1c0x115234-1600y56776-1200m2392.png", type: "", }, Replaced due to data privacy
    { id: 87, filename: "3_seed0400.png", type: "", }, 
    // { id: 88, filename: "073RE_s1c0x70644-1600y93221-1200m3259.png", type: "", }, Replaced due to data privacy
    { id: 89, filename: "3_seed0469.png", type: "", }, 
    // { id: 90, filename: "074RE_s0c0x90583-1600y24654-1200m636.png", type: "", }, Replaced due to data privacy
    // { id: 91, filename: "441RE_s3c0x155484-1600y92683-1200m997.png", type: "", }, Replaced due to data privacy
    // { id: 92, filename: "073RE_s1c0x70643-1600y92153-1200m3187.png", type: "", }, Replaced due to data privacy
    { id: 93, filename: "2_seed0889.png", type: "", }, 
    { id: 94, filename: "0_seed0718.png", type: "", }, 
    { id: 95, filename: "0_seed0616.png", type: "", }, 
    { id: 96, filename: "3_seed0856.png", type: "", }, 
   ];

// JPP data
export var jppImagesData = [
    { id: 1, filename: "2_seed0101.png", type: "", }, 
    { id: 2, filename: "1_seed0776.png", type: "", }, 
    { id: 3, filename: "2_seed0438.png", type: "", }, 
    { id: 4, filename: "3_seed0296.png", type: "", }, 
    { id: 5, filename: "1_seed0088.png", type: "", }, 
    { id: 6, filename: "0_seed0908.png", type: "", }, 
    { id: 7, filename: "2_seed0514.png", type: "", }, 
    { id: 8, filename: "0_seed0741.png", type: "", }, 
    { id: 9, filename: "2_seed0331.png", type: "", }, 
    { id: 10, filename: "1_seed0335.png", type: "", }, 
    { id: 11, filename: "3_seed0211.png", type: "", }, 
    { id: 12, filename: "2_seed0466.png", type: "", }, 
    { id: 13, filename: "1_seed0562.png", type: "", }, 
    { id: 14, filename: "1_seed0754.png", type: "", }, 
    { id: 15, filename: "0_seed0647.png", type: "", }, 
    { id: 16, filename: "0_seed0606.png", type: "", }, 
    { id: 17, filename: "1_seed0543.png", type: "", }, 
    { id: 18, filename: "0_seed0071.png", type: "", }, 
    { id: 19, filename: "3_seed0616.png", type: "", }, 
    { id: 20, filename: "3_seed0422.png", type: "", }, 
    { id: 21, filename: "3_seed0437.png", type: "", }, 
    { id: 22, filename: "3_seed0789.png", type: "", }, 
    { id: 23, filename: "2_seed0754.png", type: "", }, 
    { id: 24, filename: "3_seed0543.png", type: "", }, 
    { id: 25, filename: "0_seed0466.png", type: "", }, 
    { id: 26, filename: "0_seed0718.png", type: "", }, 
    { id: 27, filename: "2_seed0971.png", type: "", }, 
    { id: 28, filename: "2_seed0545.png", type: "", }, 
    { id: 29, filename: "1_seed0913.png", type: "", }, 
    { id: 30, filename: "2_seed1000.png", type: "", }, 
    { id: 31, filename: "3_seed0396.png", type: "", }, 
    { id: 32, filename: "2_seed0736.png", type: "", }, 
    { id: 33, filename: "3_seed0721.png", type: "", }, 
    { id: 34, filename: "3_seed0151.png", type: "", }, 
    { id: 35, filename: "2_seed0335.png", type: "", }, 
    { id: 36, filename: "0_seed0034.png", type: "", }, 
    { id: 37, filename: "2_seed0114.png", type: "", }, 
    { id: 38, filename: "1_seed0017.png", type: "", }, 
    { id: 39, filename: "0_seed0449.png", type: "", }, 
    { id: 40, filename: "3_seed0893.png", type: "", }, 
    { id: 41, filename: "2_seed0742.png", type: "", }, 
    { id: 42, filename: "0_seed0438.png", type: "", }, 
    { id: 43, filename: "2_seed0375.png", type: "", }, 
    { id: 44, filename: "2_seed0284.png", type: "", }, 
    { id: 45, filename: "0_seed1000.png", type: "", }, 
    { id: 46, filename: "2_seed0856.png", type: "", }, 
    { id: 47, filename: "3_seed0909.png", type: "", }, 
    { id: 48, filename: "1_seed0773.png", type: "", }, 
    { id: 49, filename: "0_seed0189.png", type: "", }, 
    { id: 50, filename: "1_seed0514.png", type: "", }, 
    { id: 51, filename: "0_seed0450.png", type: "", }, 
    { id: 52, filename: "0_seed0075.png", type: "", }, 
    { id: 53, filename: "0_seed0114.png", type: "", }, 
    { id: 54, filename: "3_seed0512.png", type: "", }, 
    { id: 55, filename: "0_seed0133.png", type: "", }, 
    { id: 56, filename: "0_seed0141.png", type: "", }, 
    { id: 57, filename: "2_seed0096.png", type: "", }, 
    { id: 58, filename: "0_seed0191.png", type: "", }, 
    { id: 59, filename: "1_seed0446.png", type: "", }, 
    { id: 60, filename: "2_seed0773.png", type: "", }, 
    { id: 61, filename: "3_seed0959.png", type: "", }, 
    { id: 62, filename: "1_seed0087.png", type: "", }, 
    { id: 63, filename: "1_seed0909.png", type: "", }, 
    { id: 64, filename: "0_seed0909.png", type: "", }, 
    { id: 65, filename: "1_seed0642.png", type: "", }, 
    { id: 66, filename: "3_seed0315.png", type: "", }, 
    { id: 67, filename: "3_seed0460.png", type: "", }, 
    { id: 68, filename: "1_seed0177.png", type: "", }, 
    { id: 69, filename: "1_seed0606.png", type: "", }, 
    { id: 70, filename: "1_seed0101.png", type: "", }, 
    { id: 71, filename: "2_seed0431.png", type: "", }, 
    { id: 72, filename: "0_seed0220.png", type: "", }, 
    { id: 73, filename: "2_seed0020.png", type: "", }, 
    { id: 74, filename: "1_seed0721.png", type: "", }, 
    { id: 75, filename: "3_seed0549.png", type: "", }, 
    { id: 76, filename: "3_seed0303.png", type: "", }, 
    { id: 77, filename: "2_seed0673.png", type: "", }, 
    { id: 78, filename: "3_seed0450.png", type: "", }, 
    { id: 79, filename: "0_seed0562.png", type: "", }, 
    { id: 80, filename: "2_seed0908.png", type: "", }, 
    { id: 81, filename: "3_seed1000.png", type: "", }, 
    { id: 82, filename: "1_seed0296.png", type: "", }, 
    { id: 83, filename: "0_seed0532.png", type: "", }, 
    { id: 84, filename: "0_seed0303.png", type: "", }, 
    { id: 85, filename: "1_seed0814.png", type: "", }, 
    { id: 86, filename: "0_seed0661.png", type: "", }, 
    { id: 87, filename: "3_seed0438.png", type: "", }, 
    { id: 88, filename: "1_seed0883.png", type: "", }, 
    { id: 89, filename: "2_seed0789.png", type: "", }, 
    { id: 90, filename: "3_seed0713.png", type: "", }, 
    { id: 91, filename: "1_seed0620.png", type: "", }, 
    { id: 92, filename: "0_seed0642.png", type: "", }, 
    { id: 93, filename: "1_seed0141.png", type: "", }, 
    { id: 94, filename: "3_seed0634.png", type: "", }, 
    { id: 95, filename: "1_seed0450.png", type: "", }, 
    { id: 96, filename: "2_seed0303.png", type: "", }, 
    { id: 97, filename: "2_seed0396.png", type: "", }, 
    { id: 98, filename: "1_seed0133.png", type: "", }, 
    { id: 99, filename: "3_seed0736.png", type: "", }, 
    { id: 100, filename: "3_seed0114.png", type: "", }, 
];
