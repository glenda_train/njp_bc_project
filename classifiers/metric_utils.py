# Author: Glenda Proença Train
# ----------------------------------------------------------------------------------------------------------------------
'''
metric_utils.py: code to calculate the metrics from the results

Calculations:
  - Loss
  - Precision
  - Recall
  - F1 Score
  - Accuracy
  - ROC
  - PRC
  - AUC (ROC and PRC)
'''
# ----------------------------------------------------------------------------------------------------------------------
import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, precision_recall_curve, confusion_matrix, ConfusionMatrixDisplay
from log_utils import Log
import pandas as pd

# ----------------------------------------------------------------------------------------------------------------------

id_to_class = {
    0: "0",
    1: "1",
    2: "2",
    3: "3"
}
# ----------------------------------------------------------------------------------------------------------------------

# Class that performs the metric calculations
class Metrics:
    # ------------------------------------------------------------------------------------------------------------------

    def __init__(self, root_dir="experiment"):

        # Define the paths to save the plots and csvs
        self.plots_path = os.path.join(root_dir, "plots")
        self.results_path = os.path.join(root_dir, "results")

        # To store the scores (tp, tn, fp and fn)
        self.class_scores = None

        # Creates the root
        if (not os.path.exists(root_dir)):
            os.makedirs(root_dir)

        # Creates the dir to store the plots
        if (not os.path.exists(self.plots_path)):
            os.makedirs(self.plots_path)

        # Creates the dir to store the results
        if (not os.path.exists(self.results_path)):
            os.makedirs(self.results_path)

    # ------------------------------------------------------------------------------------------------------------------

    # Function that gets the scores (TP, TN, FP and FN)
    def get_scores(self, expected, predicted, classes=None, score_threshold=0.5):
        class_scores = {}

        # Define the classes
        if(classes is None):
            classes = ["0", "1", "2", "3"]

        # Define the attributes for each class
        for class_ in classes:
            class_scores[class_] = {"tp": 0, "tn": 0, "fp": 0, "fn": 0}

        # For each class
        for class_id in range(4):

            # Get the predictions and expected values for the current class
            exps = list(expected[:, class_id])
            preds = [1.0 if elem >= score_threshold else 0 for elem in predicted[:, class_id]]

            # For each expected value and prediction
            for exp, pred in zip(exps, preds):

                # Right ones
                if(exp == pred):

                    # TP
                    if(exp == 1.0):
                        class_scores[id_to_class[class_id]]["tp"] += 1

                    # TN
                    else:
                        class_scores[id_to_class[class_id]]["tn"] += 1

                # Wrong ones
                else:

                    # FN
                    if(exp == 1.0):
                        class_scores[id_to_class[class_id]]["fn"] += 1

                    # FP
                    else:
                        class_scores[id_to_class[class_id]]["fp"] += 1

        tp_sum = 0
        tn_sum = 0
        fp_sum = 0
        fn_sum = 0

        # Calculates the overall scores
        for class_ in classes:
            tp_sum += class_scores[class_]["tp"]
            tn_sum += class_scores[class_]["tn"]
            fp_sum += class_scores[class_]["fp"]
            fn_sum += class_scores[class_]["fn"]

        class_scores["overall"] = {
            "tp": tp_sum,
            "tn": tn_sum,
            "fp": fp_sum,
            "fn": fn_sum
        }

        return(class_scores)
    # ------------------------------------------------------------------------------------------------------------------

    # Precision (each class and overall)
    def precision_score(self, expected, predicted):

        # Get the scores (tp, tn, fp and fn)
        if (self.class_scores is None):
            self.class_scores = self.get_scores(expected, predicted)

        # Get the score for each class
        class_precisions = {}
        for class_ in list(self.class_scores.keys()):

            # Calculate the score
            tp = self.class_scores[class_]["tp"] * 1.0
            fp = self.class_scores[class_]["fp"] * 1.0
            numerator = tp
            denominator = tp + fp

            # Handles the division by zero
            if (denominator == 0):
                precision = 0.0
            else:
                precision = numerator / denominator

            # Store the score
            class_precisions[class_] = precision

        return (class_precisions)
    # ------------------------------------------------------------------------------------------------------------------

    # Accuracy (each class and overall)
    def accuracy_score(self, expected, predicted):

        # Get the scores (tp, tn, fp and fn)
        if (self.class_scores is None):
            self.class_scores = self.get_scores(expected, predicted)

        # Get the score for each class
        class_accuracies = {}
        overall_numerator = 0
        overall_denominator = 0
        for class_ in list(self.class_scores.keys()):

            # Calculate the score
            tp = self.class_scores[class_]["tp"] * 1.0
            tn = self.class_scores[class_]["tn"] * 1.0
            fp = self.class_scores[class_]["fp"] * 1.0
            fn = self.class_scores[class_]["fn"] * 1.0
            numerator = tp + tn
            denominator = tp + tn + fp + fn

            # Add the numerator and denominator into the overall variables
            overall_numerator += numerator
            overall_denominator += denominator

            # Handles the division by zero
            if (denominator == 0):
                accuracy = 0.0
            else:
                accuracy = numerator / denominator

            # Store the score
            class_accuracies[class_] = accuracy

        return (class_accuracies)
    # ------------------------------------------------------------------------------------------------------------------

    # Recall (each class and overall) --> Sensibility
    def recall_score(self, expected, predicted):

        # Get the scores (tp, tn, fp and fn)
        if (self.class_scores is None):
            self.class_scores = self.get_scores(expected, predicted)

        # Get the score for each class
        class_recalls = {}
        overall_numerator = 0
        overall_denominator = 0
        for class_ in list(self.class_scores.keys()):

            # Calculate the score
            tp = self.class_scores[class_]["tp"] * 1.0
            fn = self.class_scores[class_]["fn"] * 1.0
            numerator = tp
            denominator = tp + fn

            # Add the numerator and denominator into the overall variables
            overall_numerator += numerator
            overall_denominator += denominator

            # Handles the division by zero
            if (denominator == 0):
                recall = 0.0
            else:
                recall = numerator / denominator

            # Store the score
            class_recalls[class_] = recall

        return (class_recalls)

    # ------------------------------------------------------------------------------------------------------------------

    # Specificity (each class and overall)
    def specificity_score(self, expected, predicted):

        # Get the scores (tp, tn, fp and fn)
        if (self.class_scores is None):
            self.class_scores = self.get_scores(expected, predicted)

        # Get the score for each class
        class_specificities = {}
        overall_numerator = 0
        overall_denominator = 0
        for class_ in list(self.class_scores.keys()):

            # Calculate the score
            tn = self.class_scores[class_]["tn"] * 1.0
            fp = self.class_scores[class_]["fp"] * 1.0
            numerator = tn
            denominator = tn + fp

            # Add the numerator and denominator into the overall variables
            overall_numerator += numerator
            overall_denominator += denominator

            # Handles the division by zero
            if (denominator == 0):
                specificity = 0.0
            else:
                specificity = numerator / denominator

            # Store the score
            class_specificities[class_] = specificity

        return (class_specificities)
    # ------------------------------------------------------------------------------------------------------------------

    # F1 Score (each class and overall)
    def f1_score(self, expected, predicted):

        # Get the data
        precisions = self.precision_score(expected, predicted)
        recalls = self.recall_score(expected, predicted)

        # Get the score for each class
        class_f1_scores = {}
        for class_ in list(self.class_scores.keys()):

            # Calculate the score
            precision = precisions[class_]
            recall = recalls[class_]
            numerator = 2.0 * precision * recall
            denominator = precision + recall

            # Handles the division by zero
            if (denominator == 0):
                f1_score = 0.0
            else:
                f1_score = numerator / denominator

            # Store the score
            class_f1_scores[class_] = f1_score

        return (class_f1_scores)
    # ------------------------------------------------------------------------------------------------------------------

    def prc_auc_score(self, expected, predicted, class_ids=None):
        prc_aucs = {}

        if (class_ids is None):
            class_ids = [*range(4)]

        for class_ in class_ids:
            # Get the Precision and Recall
            precision, recall, _ = precision_recall_curve(expected[:, class_], predicted[:, class_])

            # Get the AUC (Area Under the Curve)
            prc_auc = auc(recall, precision)
            prc_aucs[id_to_class[class_].lower()] = prc_auc

        return (prc_aucs)
    # ------------------------------------------------------------------------------------------------------------------

    def roc_auc_score(self, expected, predicted, class_ids=None):
        roc_aucs = {}

        if (class_ids is None):
            class_ids = [*range(4)]

        for class_ in class_ids:
            # Get the FPR (False Positive Rate) and TPR (True Positive Rate)
            fpr, tpr, _ = roc_curve(expected[:, class_], predicted[:, class_])

            # Get the AUC (Area Under the Curve)
            roc_auc = auc(fpr, tpr)
            roc_aucs[id_to_class[class_].lower()] = roc_auc

        return (roc_aucs)
    # ------------------------------------------------------------------------------------------------------------------

    def plot_roc_curve(self, expected, predicted, class_ids=None, size=10, save=None):

        # Plot Settings
        colors = ['deeppink', 'b', 'g', 'darkorange']
        fig = plt.figure(figsize=(1.1 * size, size))
        ax = plt.axes()
        ax.plot([0, 1], [0, 1], '--', lw=0.2 * size)

        # If the class to plot was not informed, plot all classes
        if (class_ids is None):
            class_ids = [*range(4)]

        # For each class
        for class_ in class_ids:
            # Get the FPR (False Positive Rate) and TPR (True Positive Rate)
            fpr, tpr, _ = roc_curve(expected[:, class_], predicted[:, class_])

            # Get the AUC (Area Under the Curve)
            roc_auc = auc(fpr, tpr)

            # Build the plot
            ax.plot(fpr, tpr, color=colors[class_], lw=0.2 * size,
                    label="ROC of Class {}".format(id_to_class[class_]) + " (AUC = {0:0.3f})".format(roc_auc))

        # Plot Settings
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate', fontsize=1.8 * size)
        ax.set_ylabel('True Positive Rate', fontsize=1.8 * size)
        if (save):
            title = save.split("_")[1]
            title = title[0].upper() + title[1:]
            ax.set_title("Receiver Operating Characteristic Curve ({})".format(title),
                         fontsize=1.8 * size, y=1.01)
        else:
            ax.set_title("Receiver Operating Characteristic Curve", fontsize=1.8 * size, y=1.01)
        ax.legend(loc="lower center", bbox_to_anchor=(0.5, -0.43), fontsize=1.5 * size)
        ax.xaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        ax.yaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        fig.subplots_adjust(bottom=0.27)

        # Show the plot
        if (save is None):
            plt.show()

        # Save the plot
        else:
            plt.savefig(os.path.join(self.plots_path, save + "_roc.png"))
    # ------------------------------------------------------------------------------------------------------------------

    def plot_prc_curve(self, expected, predicted, class_ids=None, size=10, save=None):

        # Plot Settings
        colors = ['deeppink', 'b', 'g', 'darkorange']
        fig = plt.figure(figsize=(1.1 * size, size))
        ax = plt.axes()

        # If the class to plot was not informed, plot all classes
        if (class_ids is None):
            class_ids = [*range(4)]

        # For each class
        for class_ in class_ids:
            # Get the Precision and Recall
            precision, recall, _ = precision_recall_curve(expected[:, class_], predicted[:, class_])

            # Get the AUC (Area Under the Curve)
            prc_auc = auc(recall, precision)

            # Build the plot
            ax.plot(recall, precision, color=colors[class_], lw=0.2 * size,
                    label="PRC of Class {}".format(id_to_class[class_]) + " (AUC = {0:0.3f})".format(prc_auc))

            rp = (expected[:, class_] > 0).sum() / len(expected)
            ax.plot([0, 1], [rp, rp], '--', color=colors[class_], lw=0.2 * size)

        # Plot Settings
        ax.set_xlim([-0.05, 1.05])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('Recall', fontsize=1.8 * size)
        ax.set_ylabel('Precision', fontsize=1.8 * size)
        if (save):
            title = save.split("_")[1]
            title = title[0].upper() + title[1:]
            ax.set_title("Precision-Recall Curve ({})".format(title), fontsize=1.8 * size, y=1.01)
        else:
            ax.set_title("Precision-Recall Curve", fontsize=1.8 * size, y=1.01)
        ax.legend(loc="lower center", bbox_to_anchor=(0.5, -0.43), fontsize=1.5 * size)
        ax.xaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        ax.yaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        fig.subplots_adjust(bottom=0.27)

        # Show the plot
        if (save is None):
            plt.show()

        # Save the plot
        else:
            plt.savefig(os.path.join(self.plots_path, save + "_prc.png"))
    # ------------------------------------------------------------------------------------------------------------------

    # Function to print the results (predicted and expected values) and all the metrics
    def print_all_metrics(self, expected, predicted, model_name, classes, phase, log=None, each_class=False):

        # Get the scores (tp, tn, fp and fn)
        self.class_scores = self.get_scores(expected, predicted, classes=classes)

        # Precision
        precisions = self.precision_score(expected, predicted)

        # Recall
        recalls = self.recall_score(expected, predicted)

        # Specificity
        specificities = self.specificity_score(expected, predicted)

        # F1 Score
        f1_scores = self.f1_score(expected, predicted)

        # Accuracy
        accuracies = self.accuracy_score(expected, predicted)

        # AUC ROC
        auc_roc = self.roc_auc_score(expected, predicted)

        # AUC PRC
        auc_prc = self.prc_auc_score(expected, predicted)

        # Save the ROC and PRC for each class
        if(each_class):
            for class_id in range(4):
                self.plot_roc_curve(expected, predicted, class_ids=[class_id],
                                    save="{}_{}_{}".format(model_name, phase, id_to_class[class_id].lower()))

                self.plot_prc_curve(expected, predicted, class_ids=[class_id],
                                    save="{}_{}_{}".format(model_name, phase, id_to_class[class_id].lower()))

        # Save the ROC and PRC for all classes
        self.plot_roc_curve(expected, predicted, save="{}_{}_all".format(model_name, phase))
        self.plot_prc_curve(expected, predicted, save="{}_{}_all".format(model_name, phase))

        # Print all metrics
        print("\n{} Metrics:".format(phase[0].upper() + phase[1:]))
        for index, current_class in enumerate(classes):
            print("\n  {}: ".format(current_class[0].upper() + current_class[1:]))
            print("    - TP: {}".format(self.class_scores[current_class.lower()]["tp"]))
            print("    - TN: {}".format(self.class_scores[current_class.lower()]["tn"]))
            print("    - FP: {}".format(self.class_scores[current_class.lower()]["fp"]))
            print("    - FN: {}".format(self.class_scores[current_class.lower()]["fn"]))
            print("    - Accuracy: {:.4f}".format(accuracies[current_class.lower()]))
            print("    - Precision: {:.4f}".format(precisions[current_class.lower()]))
            print("    - Recall: {:.4f}".format(recalls[current_class.lower()]))
            print("    - Specificity: {:.4f}".format(specificities[current_class.lower()]))
            print("    - F1 Score: {:.4f}".format(f1_scores[current_class.lower()]))
            print("    - AUC ROC: {:.4f}".format(auc_roc[current_class]))
            print("    - AUC PRC: {:.4f}".format(auc_prc[current_class]))

        print("\n  Overall Scores: ")
        print("    - TP: {}".format(self.class_scores["overall"]["tp"]))
        print("    - TN: {}".format(self.class_scores["overall"]["tn"]))
        print("    - FP: {}".format(self.class_scores["overall"]["fp"]))
        print("    - FN: {}".format(self.class_scores["overall"]["fn"]))
        print("    - Accuracy: {:.4f}".format(accuracies["overall"]))
        print("    - Precision: {:.4f}".format(precisions["overall"]))
        print("    - Recall: {:.4f}".format(recalls["overall"]))
        print("    - Specificity: {:.4f}".format(specificities["overall"]))
        print("    - F1 Score: {:.4f}".format(f1_scores["overall"]))
        print("-" * 50)

        # Logging ------------------------------------------------------------------------------------------------------
        if (log is not None):

            # Predictions and Expected Values
            for index, example in enumerate(predicted):
                message = "Results | {} | Example: {} | Predicted: [ {:.4f} {:.4f} {:.4f} {:.4f} ] | Expected : [ {:.4f} {:.4f} {:.4f} {:.4f} ]".format(
                    phase[0].upper() + phase[1:],
                    index, example[0], example[1], example[2], example[3], expected[index][0],
                    expected[index][1], expected[index][2],
                    expected[index][3])
                log.log_it(message)

            # Metrics (Precision, Recall, F1 Score, AUC ROC, AUC PRC, Accuracy)
            for index, current_class in enumerate(classes):
                # Scores (tp, tn, fp and fn)
                message = "Metrics | {} | {} | Scores | TP: {} | TN: {} | FP: {} | FN: {}".format(
                    phase[0].upper() + phase[1:],
                    current_class[0].upper() + current_class[1:],
                    self.class_scores[current_class.lower()]["tp"],
                    self.class_scores[current_class.lower()]["tn"],
                    self.class_scores[current_class.lower()]["fp"],
                    self.class_scores[current_class.lower()]["fn"])
                log.log_it(message)

                # Accuracy
                message = "Metrics | {} | {} | Accuracy: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                        current_class[0].upper() + current_class[1:],
                                                                        accuracies[current_class.lower()])
                log.log_it(message)

                # Precision
                message = "Metrics | {} | {} | Precision: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                         current_class[0].upper() + current_class[1:],
                                                                         precisions[current_class.lower()])
                log.log_it(message)

                # Recall
                message = "Metrics | {} | {} | Recall: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                      current_class[0].upper() + current_class[1:],
                                                                      recalls[current_class.lower()])
                log.log_it(message)

                # Specificity
                message = "Metrics | {} | {} | Specificity: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                           current_class[0].upper() + current_class[1:],
                                                                           specificities[current_class.lower()])
                log.log_it(message)

                # F1 Score
                message = "Metrics | {} | {} | F1 Score: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                        current_class[0].upper() + current_class[1:],
                                                                        f1_scores[current_class.lower()])
                log.log_it(message)

                # AUC (ROC)
                message = "Metrics | {} | {} | AUC ROC: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                       current_class[0].upper() + current_class[1:],
                                                                       auc_roc[current_class])
                log.log_it(message)

                # AUC (PRC)
                message = "Metrics | {} | {} | AUC PRC: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                       current_class[0].upper() + current_class[1:],
                                                                       auc_prc[current_class])
                log.log_it(message)

            # Scores (tp, tn, fp and fn)
            message = "Metrics | {} | Scores | TP: {} | TN: {} | FP: {} | FN: {}".format(
                phase[0].upper() + phase[1:],
                self.class_scores["overall"]["tp"],
                self.class_scores["overall"]["tn"],
                self.class_scores["overall"]["fp"],
                self.class_scores["overall"]["fn"])
            log.log_it(message)

            # Accuracy
            message = "Metrics | {} | Accuracy: {:.4f}".format(phase[0].upper() + phase[1:], accuracies["overall"])
            log.log_it(message)

            # Precision
            message = "Metrics | {} | Precision: {:.4f}".format(phase[0].upper() + phase[1:], precisions["overall"])
            log.log_it(message)

            # Recall
            message = "Metrics | {} | Recall: {:.4f}".format(phase[0].upper() + phase[1:], recalls["overall"])
            log.log_it(message)

            # Specificity
            message = "Metrics | {} | Specificity: {:.4f}".format(phase[0].upper() + phase[1:],
                                                                  specificities["overall"])
            log.log_it(message)

            # F1 Score
            message = "Metrics | {} | F1 Score: {:.4f}".format(phase[0].upper() + phase[1:], f1_scores["overall"])
            log.log_it(message)
        # --------------------------------------------------------------------------------------------------------------

    # ------------------------------------------------------------------------------------------------------------------

    # Function that plot the loss for training and validation
    def plot_loss(self, loss, save=True):
        ax = plt.axes()
        ax.plot(loss["train"], label="train")
        ax.plot(loss["test"], label="dev")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Loss")
        ax.legend()

        if (save is None):
            ax.set_title("Training and Validation Losses")
            plt.show()
        else:
            ax.set_title("Training and Validation Losses ({})".format(save))
            plt.savefig(os.path.join(self.plots_path, save + "_loss.png"))

    # ------------------------------------------------------------------------------------------------------------------

    # Plot ROC Curve (Receiver Operating Characteristic Curve)
    def plot_roc(self, expected, prediction, classes, size=10, save=True):

        # Get the FPR and TPR of each class
        roc_data = self.get_roc_curve(expected, prediction, classes)

        # Define the plot configurations
        colors = ['pink', 'c', 'deeppink', 'b', 'g', 'm', 'y', 'r', 'k']
        fig = plt.figure(figsize=(1.1 * size, size))
        ax = plt.axes()
        ax.plot([0, 1], [0, 1], '--', lw=0.2 * size)
        
        # Get the curve for each class
        for index, class_ in enumerate(classes):
            # Get the FPR and TPR of current class
            tpr = roc_data[class_]["tpr"]
            fpr = roc_data[class_]["fpr"]

            # Format the class name
            current_class = class_[0].upper() + class_[1:]

            # Plot the current class ROC curve
            ax.plot(fpr, tpr,
                    label="ROC of Class {}".format(current_class) + " (area = {0:0.3f})".format(auc(fpr, tpr)),
                    color=colors[(index + expected.shape[1]) % len(colors)],
                    linewidth=0.2 * size)

        # Plot configurations
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate', fontsize=1.8 * size)
        ax.set_ylabel('True Positive Rate', fontsize=1.8 * size)
        ax.set_title("Receiver Operating Characteristic Curve ({})".format(save), fontsize=1.8 * size, y=1.01)
        ax.legend(loc="lower center", bbox_to_anchor=(0.5, -0.43), fontsize=1.5 * size)
        ax.xaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        ax.yaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        fig.subplots_adjust(bottom=0.27)

        if (save is None):
            plt.show()
        else:
            plt.savefig(os.path.join(self.plots_path, save + "_roc.png"))
    # ------------------------------------------------------------------------------------------------------------------

    # Plot the PRC (Precision-Recall Curve)
    def plot_prc(self, expected, predicted, classes, size=10, save=True):

        # Get the precisions and recalls of each class
        prc_data = self.get_precision_recall_curve(expected, predicted, classes)

        # Define the plot configurations
        colors = ['pink', 'c', 'deeppink', 'b', 'g', 'm', 'y', 'r', 'k']
        fig = plt.figure(figsize=(1.1 * size, size))
        ax = plt.axes()

        # Plot the current class ROC curve
        for index, class_ in enumerate(classes):
            # Format the class name
            current_class = class_[0].upper() + class_[1:]

            # Get the precision and recall
            recall = prc_data[class_]["recalls"]
            precision = prc_data[class_]["precisions"]

            # Plot it
            ax.plot(recall, precision,
                    label="PRC of Class {}".format(current_class) + " (area = {0:0.3f})".format(auc(recall, precision)),
                    color=colors[(index + expected.shape[1]) % len(colors)],
                    linewidth=0.2 * size)

            label, _ = expected[index]
            rp = (label > 0).sum() / len(label)
            ax.plot([0, 1], [rp, rp], '--', color=colors[(index + expected.shape[1]) % len(colors)],
                    lw=0.2 * size)

        # Plot configurations
        ax.set_xlim([-0.05, 1.05])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('Recall', fontsize=1.8 * size)
        ax.set_ylabel('Precision', fontsize=1.8 * size)
        ax.set_title('Precision-Recall Curve {}'.format("(" + save + ")" if save is not None else ""),
                     fontsize=1.8 * size, y=1.01)
        ax.legend(loc="lower center", bbox_to_anchor=(0.5, -0.43), fontsize=1.5 * size)
        ax.xaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        ax.yaxis.set_tick_params(labelsize=1.6 * size, size=size / 2, width=0.2 * size)
        fig.subplots_adjust(bottom=0.27)

        # Show the plot
        if (save is None):
            plt.show()

        # Save the plot
        else:
            plt.savefig(os.path.join(self.plots_path, save + "_prc.png"))
    # ------------------------------------------------------------------------------------------------------------------

    # Plot the confusion matrix
    def plot_confusion_matrix(self, expected, predicted, classes, save=True):

        # Function to transform the onehot into integers (classes)
        def array_to_int(array):

            try:
                int_class = int(np.where(array == 1.0)[0][0])
            except:
                int_class = 0
            return(int_class)
        # --------------------------------------------------------------------------------------------------------------

        all_expected = []
        all_predicted = []
        # Transform all arrays in integers
        for pred, expec in zip(predicted, expected):
            int_expected = array_to_int(expec)
            int_predicted = array_to_int(pred)
            all_expected.append(int_expected)
            all_predicted.append(int_predicted)

        # Calculate the confusion matrix
        int_classes = [int(class_) for class_ in classes]
        cm = confusion_matrix(all_expected, all_predicted, labels=int_classes)

        # Build the plot
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=int_classes)
        disp.plot(cmap=plt.cm.Blues)
        plt.title("Confusion Matrix ({})".format(save if save is not None else ""))

        # Show the plot
        if(save is None):
            plt.show()

        # Save the plot
        else:
            plt.savefig(os.path.join(self.plots_path, save + "_cm.png"))
    # ------------------------------------------------------------------------------------------------------------------

    # Function to build the csv with all the results (of a specific phase)
    def results_csv(self, expected, predicted, model_name, classes, phase):

        # Get the scores (tp, tn, fp and fn)
        if (self.class_scores is None):
            self.class_scores = self.get_scores(expected, predicted)

        # AUC (ROC)
        auc_roc = self.roc_auc_score(expected, predicted)

        # AUC (PRC)
        auc_prc = self.prc_auc_score(expected, predicted)

        # Precision
        precisions = self.precision_score(expected, predicted)

        # Recall
        recalls = self.recall_score(expected, predicted)

        # Specificity
        specificities = self.specificity_score(expected, predicted)

        # F1 Score
        f1_scores = self.f1_score(expected, predicted)

        # Accuracy
        accuracies = self.accuracy_score(expected, predicted)

        # Build the csv
        results = pd.DataFrame({
            "Métricas / Classes": [id_to_class[0], id_to_class[1], id_to_class[2], id_to_class[3], "Geral"],

            "Acurácia": [accuracies[id_to_class[0]],
                         accuracies[id_to_class[1]],
                         accuracies[id_to_class[2]],
                         accuracies[id_to_class[3]],
                         accuracies["overall"]],

            "Precisão": [precisions[id_to_class[0]],
                         precisions[id_to_class[1]],
                         precisions[id_to_class[2]],
                         precisions[id_to_class[3]],
                         precisions["overall"]],

            "Sensibilidade / Recall": [recalls[id_to_class[0]],
                                       recalls[id_to_class[1]],
                                       recalls[id_to_class[2]],
                                       recalls[id_to_class[3]],
                                       recalls["overall"]],

            "Especificidade": [specificities[id_to_class[0]],
                               specificities[id_to_class[1]],
                               specificities[id_to_class[2]],
                               specificities[id_to_class[3]],
                               specificities["overall"]],

            "F1-Score": [f1_scores[id_to_class[0]],
                         f1_scores[id_to_class[1]],
                         f1_scores[id_to_class[2]],
                         f1_scores[id_to_class[3]],
                         f1_scores["overall"]],

            "AUC ROC": [auc_roc[id_to_class[0]],
                        auc_roc[id_to_class[1]],
                        auc_roc[id_to_class[2]],
                        auc_roc[id_to_class[3]],
                        0.0],

            "AUC PRC": [auc_prc[id_to_class[0]],
                        auc_prc[id_to_class[1]],
                        auc_prc[id_to_class[2]],
                        auc_prc[id_to_class[3]],
                        0.0]
        })

        # Write the csv
        results.to_csv("{}".format(os.path.join(self.results_path, phase + "_" + model_name + "_results.csv")),
                       index=False)
    # ------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------