# Quantitative Analysis

* In order to evaluate the quality of images generated by StyleGAN2ADA, we propose a form of quantitative analysis with the DreamSIM metric.

* The flow can be seen in the figure below, in which we selected 100 images from each class (step 1) and compared each of them with the training set (step 2 and 3).

* For each generated image, we get the name of training set image that obtained the lowest DreamSIM value in relation to the generated image (step 4).

* The results are stored in a csv, with the minimum and maximum values, in addition to the image and class names.

![quantitative analysis flow](../../readme_images/dreamsim_flow.png "Quantitative Analysis Flow")

## File Organization

|                            Filename                            |                                               Description                                                |
|:--------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------:|
|           [calc_metrics.ipynb](calc_metrics.ipynb)             | File that calculates the SSIM and DreamSIM metrics of each generated image compared to the training set. |
| [output_calc_dreamsim_0_50.csv](output_calc_dreamsim_0_50.csv) |               File that contains the DreamSIM outputs for the selected generated images.                 |

## How to Install the Dependencies

* You can follow the instructions in the first cell of [calc_metrics.ipynb](calc_metrics.ipynb) notebook.

* The goal is to create an environment, add it to the notebooks as kernels and install the project dependencies.