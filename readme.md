# Generation of Synthetic Data to Improve Breast Cancer IHQ Images

* **Authors:**
    * Glenda Proença Train
    * Johanna Elisabeth Rogalsky
    * Sergio Ossamu Ioshii
    * Milton Mader de Bittencourt Neto
    * Milena Massumi Kozonoe
    * Lucas Ferrari de Oliveira

* Due to the rapid development of technology in the last decade, the pathology field began its digital era with the difusion of WSIs (Whole Slide Images).

* With this advancement, providing reliable automated diagnostics has become highly desirable due to its ability to reduce specialist's time and effort on time-consuming and exhausting tasks.

* A progress is being made with Machine Learning and Deep Learning techniques, but there are still many limitations caused by:
    * Scarcity
    * Class imbalance
    * Lack of public availability of medical data
    <br/>

* To mitigate these problems, we proposed a methodology to investigate the impact of adding synthetic medical images to classify breast cancer IHC images.

* We chose the images of Estrogen Receptor biomarkers annotated with the staining **intensity score (IS)** that presents the following values:
    * **0** &ensp; **->** negative cells
    * **1+** &nbsp;**->** weakly positive cells
    * **2+** &nbsp;**->** moderately positive cells
    * **3+** &nbsp;**->** strongly positive cells
    <br/> 

* An example is presented in **a)** in the figure below.

![Real and Generated Images](readme_images/real_and_gen_images.png "Real and Generated Images")

* We performed the **classification** on portions of the WSIs (called patches) and considered the following classifiers:
    * **SVM** | Support Vector Machine*
    * **CNN** | Convolutional Neural Network**
    * **DenseNet** | Densely Connected Convolutional
Network***
    * **ViT** | Vision Transformer****

    <br/>

* We used **StyleGAN2ADA**\***** to perform the generation of synthetic medical data (to generate new patches). 
    * Examples in **b)** in the figure above.
    <br/>

* The experiments covered **class balancing (E4 and E5)** and the addition of a **small amount of synthetic images to the training set (E2 and E3)**, improving the classification result by up to 17 percentage points in f1-score.

* Futhermore, we implemented a **quantitative analysis** of the synthetic images, with a result of 0.091 of average DreamSIM, demonstrating a high similarity between the generated images and the real ones.

* Finally, we carried out a **qualitative analysis** using the Rapid Scene Categorization method, involving the participation of 3 pathologists.

* In conclusion, the synthetic images were not detected by pathologists, indicating that the images present a high quality standard.

## Directory Organization

|               Filename                |                                   Description                                    |
|:-------------------------------------:|:--------------------------------------------------------------------------------:|
| [database/](database) | Directory that contains the dataset (images and csv). |
| [preprocessing/](preprocessing) | Directory that contains notebooks to build the training, validation and test sets and add synthetic images to the training set. |
| [classifiers/](classifiers) | Directory that contains the classifiers (based on IS score classes). |
| [stylegan/](stylegan) | Directory that contains the files responsible for preprocessing and running the StyleGAN2ADA. |
| [autoaugment/](autoaugment) | Directory that contains the files responsible for running the AutoAugment. |
| [image_quality_analysis/](image_quality_analysis) | Directory that contains notebooks responsible for quantitative and qualitative analysis of the generated images. |           

## References

\*Rogalsky, J. E. (2021). **Semi-automatic ER and PR scoring in immunohistochemistry H-DAB breast cancer images**. https://acervodigital.ufpr.br/handle/1884/73470.

\*\*Tang, Z., Chuang, K.V., DeCarli, C. et al. (2019). **Interpretable classification of Alzheimer’s disease pathologies with a convolutional neural network pipeline**. Nat Commun 10, 2173 (2019). https://doi.org/10.1038/s41467-019-10212-1

\*\*\*Huang, G., Liu, Z., Maaten, L.V.D., Weinberger, K.Q., (2017). **Densely connected convolutional networks**.
https://doi.ieeecomputersociety.org/10.1109/CVPR.2017.243

\*\*\*\*Dosovitskiy, A., Beyer, L., Kolesnikov, A., Weissenborn, D., Zhai, X., Unterthiner, T., Dehghani, M., Minderer, M., Heigold, G., Gelly, S., Uszkoreit, J., Houlsby, N., (2021). **An image is worth 16x16 words: Transformers for image recognition at scale.** http://arxiv.org/abs/2010.11929

\*\*\*\*\*Karras, T., Aittala, M., Hellsten, J., Laine, S., Lehtinen, J. and Aila, T. (2020). **Training Generative Adversarial Networks with Limited Data**.
https://arxiv.org/abs/2006.06676