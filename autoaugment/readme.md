# AutoAugment

* The AutoAugment formulates the problem of finding the best augmentation policy as a discrete search problem.

* Initially, the search space is defined by a sampling of augmentation techniques, which establish information about:
    * Which image processing operation to use
    * The probability of applying this technique and
    * The magnitude of the operation.

* Then, the search algorithm applies Reinforcement Learning to discover the best augmentation techniques for the problem.

## File Organization

|      Filename              |                                   Description                                      |
|:--------------------------:|:----------------------------------------------------------------------------------:|
| [model.ipynb](model.ipynb) |  Notebook that contains all the steps to generate new imagens with AutoAugment. |