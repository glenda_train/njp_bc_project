# StyleGAN2ADA

* This directory contains the files responsible for preprocessing and running the StyleGAN2ADA.

* The image below demonstrates how we used StyleGAN2ADA to generate new images in this research.

* Initially, we separate the training set (see [this readme](../preprocessing/readme.md)).

* Then, the training set was divided by class, with each folder responsible for storing images from only 1 class (step 1).

* Next, we train specific StyleGAN2ADAs to generate images from each class (step 2 and 3).

* Finally, each of these networks generated images (step 4) and these images were added to the original training set (step 5).

* For more detailed information, access the notebooks in this repository.

![StyleGAN flow](../readme_images/stylegan_flow.png "StyleGAN Flow")

## File Organization

|               Filename                     |                                   Description                                    |
|:------------------------------------------:|:--------------------------------------------------------------------------------:|
| [preprocessing.ipynb](preprocessing.ipynb) | File that preprocess the training set, divinding it into specific folders per class.  |
| [stylegan2ada.ipynb](stylegan2ada.ipynb)   | File that contains the information to install and run the StyleGAN2ADA. |

## Directory Organization

|               Filename                     |                                   Description                                    |
|:------------------------------------------:|:--------------------------------------------------------------------------------:|
| [trained_weights/](trained_weights) | Directory that contains the trained StyleGAN2ADA models for each class used in the research.  |