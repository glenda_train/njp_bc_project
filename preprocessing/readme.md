# Split Dataset and Add StyleGAN2ADA and AutoAugment Images Into the Training Set

* This repository presents the codes used to deal with the csvs that represents the datasets.

* Training set.

* Validation set.

* Test set.

* **E2** | Training set + 100 synthetic images from each class (AutoAument).

* **E3** | Training set + 100 synthetic images from each class (StyleGAN2ADA).

* **E4** | Balanced training set with 1000 samples of each class (AutoAugment). 

* **E5** | Balanced training set with 1000 samples of each class (StyleGAN2ADA).

## File Organization

| Filename  | Description |
|:---------:|:-----------:|
| [split_data.ipynb](split_data.ipynb) | Code that divides the data set into training (80%), validation (10%) and test (10%) sets. |
| [build_experiments.ipynb](build_experiments.ipynb) | Code that adds synthetic images to the training set. |
| [RE_original_train.csv](RE_original_train.csv) | Csv that represents the training set. |
| [RE_original_dev.csv](RE_original_dev.csv) | Csv that represents the validation set. |
| [RE_original_test.csv](RE_original_test.csv) | Csv that represents the test set. |
| [RE_aa_100_add_samples_train.csv](RE_100_aa_add_samples_train.csv) | Csv that represents the experiment E2 (see the notebook). |
| [RE_sg_100_add_samples_train.csv](RE_100_sg_add_samples_train.csv) | Csv that represents the experiment E3 (see the notebook). |
| [RE_aa_1000_balanc_samples_train.csv](RE_1000_aa_balanc_samples_train.csv) | Csv that represents the experiment E4 (see the notebook). |
| [RE_sg_1000_balanc_samples_train.csv](RE_1000_sg_balanc_samples_train.csv) | Csv that represents the experiment E5 (see the notebook). |