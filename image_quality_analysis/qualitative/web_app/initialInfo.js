import { saveInLocalStorage, readFromLocalStorage } from "./utilities";

export function showNameForm() {

    // Build the page (user form)
    const form = document.getElementsByTagName("main")[0];
    form.innerHTML = `
    <form id="form-name" class="flex justify-center items-center h-[calc(100vh-88px)] text-lg">
      <section id="participant-data" class="bg-white p-6 rounded-lg">
        <div class="flex flex-col justify-center items-center">
          <p class="mb-5 font-bold">Informe seu Nome:</p>
          <label for="user-name"></label>
          <input type="text" id="user-name" placeholder="Digite seu nome aqui" style="text-align: center" class="w-96" required>
          <button id="send-name-button"
            class="text-lg bg-slate-950 text-slate-200 font-bold rounded-xl h-12 w-96 mt-6 hover:bg-slate-600 hover:text-slate-200">
            Enviar
          </button>
        </div>
      </section>  
    </form>
    `

    // Go to the next step
    document.addEventListener("submit", (event) => showMethodChoices(event));
}

function showMethodChoices(event) {
    event.preventDefault();

    // Get the form info
    let userName = document.getElementById("user-name");

    // To prevent double submit
    if(userName == null) {
        return;
    }
    userName = userName.value;
    console.log(userName);
    
    // Save it
    saveInLocalStorage("userName", userName);

    // Build the new page (qualitative methods)
    const methodChoices = document.getElementById("form-name");
    methodChoices.innerHTML = `
    <section id="method" class="flex flex-col items-center justify-center h-[calc(100vh-88px)]">
      <div class="flex flex-col p-10 bg-white rounded-xl">
        <p class="text-xl font-bold mb-3">Informe o Tipo de Avaliação Qualitativa que Deseja Realizar:</p>
        <div id="method-buttons" class="flex justify-center gap-5 text-lg mt-6">
            <button id="crc-button" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-76 p-3 px-4 hover:bg-slate-600 hover:text-slate-200">Categorização Rápida de Cenas</button>
            <button id="jpp-button" class="bg-slate-950 text-slate-200 font-bold rounded-xl h-14 w-76 p-3 px-4 hover:bg-slate-600 hover:text-slate-200">Julgamento por Preferência</button>
        </div>
      </div>
    </section>
    `;

    // Fast Categorization of Scenes (CRC)
    document.getElementById("crc-button").addEventListener("click", () => goToCrcPage());

    // Judgment by Preference (JPP)
    document.getElementById("jpp-button").addEventListener("click", () => goToJppPage());
}

function goToCrcPage() {

    // Save the method
    const method = "CRC";
    saveInLocalStorage("method", method);

    // Go to the method page
    window.location.href = window.location.origin + "/metodoCRC.html";
}

function goToJppPage() {

    // Save the method
    const method = "JPP";
    saveInLocalStorage("method", method);
    console.log(window.location.origin + "/metodoJPP.html");

    // Go to the method page
    window.location.href = window.location.origin + "/metodoJPP.html";
}

showNameForm();