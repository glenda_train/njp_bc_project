# Author: Glenda Proença Train
# ----------------------------------------------------------------------------------------------------------------------
''' 
log_utils.py: code to define the logging setup
'''
# ----------------------------------------------------------------------------------------------------------------------
import os
import logging
from datetime import datetime
# ----------------------------------------------------------------------------------------------------------------------

# Class to define the log structure and print the messages
class Log:
    # ------------------------------------------------------------------------------------------------------------------

    # Init the log file and log some information
    def __init__(self, save_dir="experiment", filename="log", log=True):

        if(log):
            logs_dir = os.path.join(save_dir, "logs")

            # Creates the root dir
            if (not os.path.exists(save_dir)):
                os.makedirs(save_dir)

            # Creates the dir to store the logs
            if (not os.path.exists(logs_dir)):
                os.makedirs(logs_dir)
            
            # Init the log 
            logger = logging.getLogger()

            # Define the format
            formatter = logging.Formatter('[%(levelname)s] | %(message)s')

            # Setup file handler
            fhandler  = logging.FileHandler(filename="{}.log".format(os.path.join(logs_dir, filename)))
            fhandler.setLevel(logging.INFO)
            fhandler.setFormatter(formatter)
            logger.addHandler(fhandler)
            logger.setLevel(logging.INFO)

            logging.info("Log File for Model: {}".format(filename))
            logging.info("Time | Message")

        self.log = log
    # ------------------------------------------------------------------------------------------------------------------

    # Function that logs a message with the current date and time
    def log_it(self, message):
        if(self.log):
            logging.info("{} | {}".format(datetime.now().strftime("%d-%m-%Y %H:%M:%S"), message))
    # ------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------